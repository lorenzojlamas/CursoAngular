import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})

export class ArtistaComponent implements OnInit {
  artista: any = {};
  loading:Boolean = true;
  topTraks: any[] =[];

  constructor(private router: ActivatedRoute,
              private spotify: SpotifyService) { 
    this.router.params.subscribe(params=>{
      this.getArtista(params['id']);
      this.getTopTraks(params['id']);
    })
  }
  getArtista(id:string){
    this.spotify.getArtista(id)
    .subscribe(artista => {
      this.artista = artista;
      this.loading = false;
    })
  };
  getTopTraks(id:string){
    this.spotify.getTopTraks(id)
    .subscribe(topTraks =>{
      this.topTraks = topTraks;
    });
  }
  ngOnInit() {
  }

}
