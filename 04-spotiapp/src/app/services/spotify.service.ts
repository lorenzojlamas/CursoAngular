import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {}
  
  getQuery(query:string){
    const URL= `https://api.spotify.com/v1/${ query }`; //https://api.spotify.com/v1/browse/new-releases?limit=20
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCN8rZe4-MNRQH4Qpdp7kizJLTWEz8WWJBTq2k8KC3dUHVs-EM6B4CYfp7t9x_VLq4aFdGUeGSfvueQPYE'
    });
    return this.http.get(URL,{headers})
  }

  getNewReleases(){
    return this.getQuery('browse/new-releases?limit=20')
      .pipe(map(data =>{
        return data['albums'].items
      }))    
  };

  
  getArtistas(termino: string){
    return this.getQuery(`search?q=${termino}&type=artist&limit=20`)
    .pipe(map(data => data['artists'].items)) 
  };

  getArtista(termino: string){
    return this.getQuery(`artists/${termino}`)
    //.pipe(map(data => data)) 
  };
  getTopTraks(termino: string){
    return this.getQuery(`artists/${termino}/top-tracks?country=ES`) //https://api.spotify.com/v1/artists/{id}/top-tracks
    .pipe(map(data => data['tracks']));
  };


}
