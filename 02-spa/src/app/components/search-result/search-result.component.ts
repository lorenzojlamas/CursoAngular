import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesService, Heroe } from '../../servicios/heroes.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html'
})
export class SearchResultComponent implements OnInit {
  
  heroes:any[] = [];
  termino:string;

  constructor(private activateRoute: ActivatedRoute,
              private _heroesService: HeroesService) 
    {
    }
    
    ngOnInit() {
      this.activateRoute.params.subscribe(params => {
        this.heroes = this._heroesService.buscarHeroes(params['termino']);
        this.termino = params['termino'];
      }); 
  }
}

//var heroesArr = this._heroesService.buscarHeroes(termino);