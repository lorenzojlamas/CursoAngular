# CursoAngular

## 00 - Introducción a TyScript
    Manejo básico de Ty, con su consola y la consola de angular Cli

## 01 - Hola mundo

## 02 - SPA
    Single Page, manejo de rutas, carga dinámica etc.
## 03 - Manejo de Pipes
    Creación de Pipes propios
## 04 - SpotiApp 
    Consumo de apps externas, solucionar CORS

## 05 - DeseOs - Ionic
    Manejo básico de Ionic, Pipes impuros

## 06 - Miselaneos
    Manejo de rutas hijas, Ciclo de vida de un componente.

## 07 - AuthApp