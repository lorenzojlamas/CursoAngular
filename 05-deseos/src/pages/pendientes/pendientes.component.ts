<<<<<<< HEAD
import { Component } from "@angular/core";
import { DeseosService } from '../../services/deseos.service';
import { Lista } from '../../models/lista.model';
import { NavController } from "ionic-angular";
import { AgregarPage } from '../agregar/agregar.component';
import { AlertController } from 'ionic-angular';


@Component({
    selector:'page-pendientes',
    templateUrl:'pendientes.component.html'
})
export class PendientesPage{
    constructor(public deseosService:DeseosService,
                private navCtrl:NavController,
                public alertCtrl: AlertController) {
        
    }
    listaSeleccionada(lista:Lista){
        console.log(lista);
    }
    agregarLista(){
        //
        const alerta = this.alertCtrl.create({
            title: 'Nueva Lista',
            inputs: [
              {
                name: 'titulo',
                placeholder: 'Nombre'
              },
            ],
            buttons: [
              {
                text: 'Cancel'
              },
              {
                text: 'Save',
                handler: data => {
                    if( data.titulo.length ===0){
                        return;
                    }
                    this.navCtrl.push( AgregarPage,{
                        titulo: data.titulo
                    } );
                }
              }
            ]
          });
          alerta.present();
    }

    showPrompt() {
        const prompt = this.alertCtrl.create({
          title: 'Login',
          message: "Enter a name for this new album you're so keen on adding",
          inputs: [
            {
              name: 'title',
              placeholder: 'Title'
            },
          ],
          buttons: [
            {
              text: 'Cancel',
              handler: data => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Save',
              handler: data => {
                console.log('Saved clicked');
              }
            }
          ]
        });
        prompt.present();
      }
}

=======
import { Component } from "@angular/core";
import { DeseosService } from '../../services/deseos.service';
import { Lista } from '../../models/lista.model';
import { NavController } from "ionic-angular";
import { AgregarPage } from '../agregar/agregar.component';
import { AlertController } from 'ionic-angular';


@Component({
    selector:'page-pendientes',
    templateUrl:'pendientes.component.html'
})
export class PendientesPage{
    constructor(public deseosService:DeseosService,
                private navCtrl:NavController,
                public alertCtrl: AlertController) {
                  
    }
    listaSeleccionada(lista:Lista){
      this.navCtrl.push(AgregarPage, {
        titulo: lista.titulo,
        lista:lista
      } );
    }
    agregarLista(){
        //
        const alerta = this.alertCtrl.create({
            title: 'Nueva Lista',
            inputs: [
              {
                name: 'titulo',
                placeholder: 'Nombre'
              },
            ],
            buttons: [
              {
                text: 'Cancel'
              },
              {
                text: 'Save',
                handler: data => {
                    if( data.titulo.length ===0){
                        return;
                    }
                    this.navCtrl.push( AgregarPage,{
                        titulo: data.titulo
                    } );
                }
              }
            ]
          });
          alerta.present();
    }
    borrarLista(lista:Lista){
      this.deseosService.borrarLista(lista);
    }
}

>>>>>>> 02195e645a3bf51d6978f2f0dc2a7a31a49111b7
