import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Heroe } from '../interfaces/heroe.interface';

// Hasta ahora la que menos problema me trajo, el import derecho no me permitía hacer nada
// npm install rxjs@6 rxjs-compat@6 --save

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  heroesURL:string ="https://heroesapp-a3cbc.firebaseio.com/Heroes.json"
  heroeURL:string ="https://heroesapp-a3cbc.firebaseio.com/Heroes"
  
  
  constructor(private http:Http) { }

  nuevoHeroe(heroe:Heroe){
    let body = JSON.stringify(heroe);
    let headers = new Headers({'Content-Type':'application/json'});
    return this.http.post(this.heroesURL,body,{headers}).map(res=>{
        console.log(res.json());
        return res.json();
      });
  }
  actualizarHeroe(heroe:Heroe, key$:string){
    let body = JSON.stringify(heroe);
    let headers = new Headers({'Content-Type':'application/json'});

    let url = `${ this.heroeURL }/${ key$ }.json`;
    return this.http.put(url,body,{headers}).map(res=>{
        console.log(res.json());
        return res.json();
      });
  }

  getHeroe(key$:string){
    let url= `${this.heroeURL}/${key$}.json`

      
  }
  getHeroes(key$:string){
    return this.http.get(this.heroesURL).map(res=>res.json());
  }

  borrarHeroe(key$:string){
    let url = `${ this.heroeURL }/${ key$ }.json`;
    return this.http.delete(url).map(res=>res.json())
  }
}
// https://heroesapp-a3cbc.firebaseio.com/Heroes.json