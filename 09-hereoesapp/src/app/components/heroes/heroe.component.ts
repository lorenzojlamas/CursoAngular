import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";

import { Heroe } from '../../interfaces/heroe.interface';
import { HeroesService } from '../../services/heroes.service';
import { subscribeOn } from '../../../../node_modules/rxjs/operators';
import { subscribeOn } from '../../../../node_modules/rxjs/operators';
// import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { routerNgProbeToken } from '../../../../node_modules/@angular/router/src/router_module';

import { Router, ActivatedRoute } from "@angular/router";



@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe:Heroe = {
    nombre:"",
    bio:"",
    casa:"Marvel"
  }
  
  nuevo: boolean = false;
  id:string;
  constructor(private _HeroesService: HeroesService,
              private router:Router,
              private route:ActivatedRoute) {
    this.route.params.subscribe(parametros=>{
        this.id = parametros['id'];
        if (this.id !=="nuevo") {
          this._HeroesService.getHeroe(this.id).subscribe(heroe=> this.heroe = heroe)
        }
    });
  }

  ngOnInit() {
  }

  guardar(){
    if(this.id == "nuevo"){
      this._HeroesService.nuevoHeroe(this.heroe).subscribe(data=>{
        this.router.navigate(['/heroe',data.name]);
      },
      error=> console.error(error)
    );
    
  }else{
      this._HeroesService.actualizarHeroe(this.heroe, this.id).subscribe(data=>{
        // this.router.navigate(['/heroe',data.name]);
        console.log(data);
        },
        error=> console.error(error)
    );  }
  }

  agregarNuevo(forma: NgForm){
    this.router.navigate(['/heroe','nuevo']);
    forma.reset({
      casa:"Marvel"
    });
  
  }



}
