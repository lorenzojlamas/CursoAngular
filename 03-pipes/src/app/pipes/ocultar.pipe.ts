import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ocultar'
})
export class OcultarPipe implements PipeTransform {

  transform(value: string, ocultar:boolean): string {
    
    if (ocultar){
      value = value.replace(/./g,'*');
    }
    return value;
  }

}
