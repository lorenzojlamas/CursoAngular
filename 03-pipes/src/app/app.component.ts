import { Component } from '@angular/core';
import { Heroe } from '../../../02-spa/src/app/servicios/heroes.service';
import { resolve } from 'dns';
import { reject } from '../../node_modules/@types/q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app'

  nombre="Lorenzo";
  
  nombre2="loreNzo jOsé lamas";

  arreglo = [1,2,3,4,5,6,7,8,9,10];
  PI = Math.PI;

  a = 0.234;

  salario = 1234.56;

  heroe={
    nombre:"Logan",
    clave:"Wolverin",
    edad:500,
    direccion:{
      caller:"primera",
      casa:"18"
    }
  }

  valorDePromesa = new Promise((resolve,reject)=>{
    setTimeout(() => resolve('llego la data'), 3500);
  });

  fecha = new Date();

  video:string= 'NPCabaB9UOA?rel=0';
  activar:boolean = true;
}
