import { Component, OnInit, OnChanges, AfterContentInit, AfterContentChecked,AfterViewInit,AfterViewChecked,OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `

    <app-ng-style>
    <br>

    </app-ng-style>
    <br>

    <app-css></app-css>
    <br> 
     <app-clases></app-clases>
     <br> 
     <app-ng-switch></app-ng-switch>
     
     <p [appResaltado]="'orange'">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti tempore, neque incidunt nesciunt dicta error quisquam libero eligendi laboriosam quis eum ea, necessitatibus harum suscipit quaerat, voluptate velit similique? Eaque.
    </p> 
    <br><br><br><br>
  `,
  styles: []
})
export class HomeComponent implements OnInit, OnChanges, AfterContentInit, AfterContentChecked,AfterViewInit,AfterViewChecked,OnDestroy {

  constructor() {
    console.log("Constructor");
   }

  ngOnInit() {
    console.log("ngOnInit");    
  };

  ngOnChanges (){
    console.log("ngOnChanges");
  };

  ngAfterContentInit (){
    console.log("ngAfterContentInit");
  };

  ngAfterContentChecked (){
    console.log("ngAfterContentChecked");
  };

  ngAfterViewInit (){
    console.log("ngAfterViewInit");
  };

  ngAfterViewChecked (){
    console.log("ngAfterViewChecked");
  };

  ngOnDestroy (){
    console.log("ngOnDestroy");
  };

}
